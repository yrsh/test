module .exports = {
    sys_channel: "sys",
    message_queue: "msg_queue",
    error_queue: "err_queue",
    clientListTerm: "sysCL_",
    watchInterval: 2000,
    prefixes: {
        worker: "wrk",
        sys: "sys",
        data: "data",
        generator: "wr",
        reader: "rd"
    }
};