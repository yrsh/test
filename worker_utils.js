function exec(client, cmd, args) {
    var _args = args || [];
    return new Promise(function (resolve, reject) {
        client.send_command(cmd, _args, function (err, result) {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

function parseClientList(str) {
    var list = [];
    str = str.split('\n');
    str.pop();
    str.map(function (item) {
            return item.split(' ');
        }).map(function (item) {
            var obj = {};
            for (var i=0; i < item.length; i++) {
                var it = item[i].split('=');
                obj[it[0]] = it[1];
            }
            list.push(obj);
        });
    return list;
}

function analyzeClients (objs, prefixes) {
    var info = {
            allConnections: objs.length,
            workers: {},
            generators: {},
            count: {
                workers: 0,
                generators: 0
            }
        };
    if (!info) {
        return info;
    } else {
        for (var i = 0; i < info.allConnections; i++) {
            if (objs[i].name.indexOf(prefixes.worker) >= 0) {
                var type = objs[i].name.split('_');
                var wtype = (type[1] === prefixes.generator) ? 'generators' : 'workers';
                info[wtype][type[3]] = info[wtype][type[3]] || {};
                if (type[2] === prefixes.sys) {
                    info[wtype][type[3]].sys = true;
                } else if (type[2] === prefixes.data) {
                    info[wtype][type[3]].data = true;
                }
            }
        }
        info.count.workers = Object.keys(info.workers).length;
        info.count.generators = Object.keys(info.generators).length;
        return info;
    }
}

module.exports = {
    exec: exec,
    parseClientList: parseClientList,
    analyzeClients: analyzeClients
};