var parseArgs = require('minimist');
var redis = require("redis");
var events = require('events');
var wu = require('./worker_utils');
var config = require('./worker_config');
var sysCl = redis.createClient();
var dataCl = redis.createClient();
var async = require('async');
var args = parseArgs(process.argv.slice(2));
var fs = require('fs');
var commands = [];

var TOTAL = 0, PROCESSED = 0, SEND = 0, LIMIT, INTERVAL, LOG = false;
//---------------------------------------------------
function getMessage(){
    this.cnt = this.cnt || 0;
    return this.cnt++;
}
function eventHandler(msg, callback){
    TOTAL++;
    function onComplete(){
        PROCESSED++;
        var error = Math.random() > 0.85;
        callback(error, msg);
    }
    setTimeout(onComplete, Math.floor(Math.random()*1000));
}

//for dev
var ID = Date.now();
function log (msg) {
    if (LOG) {
        console.log(ID + ' | ' + msg);
    }
}
//---------------------------------------------------


var worker = {

    ID: ID,

    alive: true,

    anyGenerator: function () {
        return new Promise(function (resolve, reject) {
            dataCl.send_command("client", ["list"], function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    var workersInfo = wu.analyzeClients(wu.parseClientList(result), config.prefixes);
                    if (workersInfo.count.workers > 1 && workersInfo.count.generators === 0) {
                        resolve(result);
                    } else {
                        reject("generator alive or locked");
                    }
                }
            });
        });
    },

    checkWorkers: function () {
        this.anyGenerator().then(() => {
            dataCl.setnx("G_LOCK", worker.ID, (err, res) => {
                log(res);
                if (err) {
                    log(err);
                } else if (Number(res)) {
                    log('setting generator');
                    this.setGenerator();
                } else {
                    log(res)
                }
            })
        }).catch((e) => {log(e)});
    },

    setGenerator: function () {
        async.series([
            (callback) => {
                log('rename');
                dataCl.client("setname", "wrk_wr_sys_" + worker.ID,
                    (err, res) => {
                        (err) ? callback(err, null) : callback(null, res);
                    });
            },
            (callback) => {
                sysCl.unsubscribe((err, res) => {
                    if (err) {
                        log(err);
                        callback(err,null);
                    } else {
                        log("unsubscribed");
                        callback(null,res);
                    }
                });
            },
            (callback) => {
                log('removing lock');
                dataCl.del("G_LOCK",
                    (err, res) => {
                        (err) ? callback(err, null) : callback(null, res);
                        clearInterval(this.watchPolling);
                    });
            }
        ], (err, results) => {
            if (err) {
                log(err);
            } else {
                log(results);
                sysCl.client("setname", "wrk_wr_sys_" + worker.ID, (_err, _res) => {
                    if (_err) {
                        log(_err);
                    } else {
                        log("IS GENERATOR");
                        this.makeGenerator();
                    }
                });
            }
        });
    },

    makeGenerator: function () {
        var i =0;
        setInterval(function () {
            dataCl.rpush(config.message_queue, getMessage(), function (err, reply) {
                if (err) {
                    console.log(err)
                } else {
                    sysCl.publish(config.sys_channel, "msg_");
                    i++;
                    if (i >= LIMIT) {
                        sysCl.publish(config.sys_channel, "STOP_");
                        console.log('SENDED: ' + i);
                        process.exit();
                    }
                }
            });
        }, INTERVAL);
    },

    processMessage: function () {
        dataCl.lpop(config.message_queue, (err, response) => {
            if (err) {
                log(err);
            } else if (response) {
                log('processing message');
                eventHandler(response, this.eventHandlerCallback);
            }
        });
    },

    eventHandlerCallback: function (error, msg) {
        if (error) {
            dataCl.rpush(config.error_queue, msg, (err, res) => {
                if (err) {
                    log(err);
                } else {
                    SEND++;
                }
            });
        }
    },

    listenSys: function () {
        sysCl.on("message", (cahnnel, message) => {
            if (message.indexOf('_')) { //cant use standart JSON.parse because try-catch perfomance
                var msg = message.split('_');
                switch (msg[0]) {
                    case "msg": //new message
                        if (this.alive) {
                            this.processMessage();
                        }
                        break;
                    case "STOP":
                        this.alive = false;
                        setTimeout(() => {
                            console.log(TOTAL, PROCESSED, SEND);
                            process.exit();
                        }, 2000);
                        break;
                }
            }
        });
    },

    watch: function () {
        this.watchPolling = setInterval(() => {
            this.checkWorkers();
        }, config.watchInterval);
    },

    onRegister: function () {
        this.multi = dataCl.multi();
        this.checkWorkers();
        this.watch();
        this.listenSys();
    }


};

function init () {
    Promise.all([
        wu.exec(dataCl, "client", ["setname", "wrk_rd_data_" + worker.ID]),
        wu.exec(sysCl, "subscribe", [config.sys_channel])
    ]).then(function (results) {
        console.log(results);
        worker.onRegister();
    }).catch(function (err) {
        log(err);
    })
}

//------------------------------------------------------
function start () {
    function cmdExec (cmd, args, callback) {
        var _args = args || [];
        dataCl.send_command(cmd, _args, function (err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }

    //reset local redis
    if (args.rr) {
        commands.push(function (callback) {
            return cmdExec("flushall", [], callback);
        });
    }

    //get errors
    if (args.ge) {
        commands.push(function (callback) {
            return cmdExec("lrange", [config.error_queue, 0, -1], callback);
        });
        commands.push(function (callback) {
            return cmdExec("del", [config.error_queue], callback);
        });
    }

    async.series(commands, function (err, results) {
        if (err) {
            console.log(err);
        } else {
            if (args.start) {
                LIMIT = Number(args.lim)|| 10;
                INTERVAL = (args.i == 0) ? 0 : Number(args.i) || 500;
                LOG = Number(args.log) || false;
                init();
                console.log('start worker')
            } else if (args.ge) {
                fs.writeFile("errors.txt", results[0], function(err) {
                    if(err) {
                        return console.log(err);
                    }
                    console.log("Errors was saved!");
                });
            } else if (args.rr) {
                console.log("base reseted");
            }
        }
    });
}
start();
